package org.nodexy.groovyLang.designPatterns.chainOfResponsibility

interface ILister {
    void listFiles(String dir)
}

class UnixLister implements ILister {
    private nextInLine
    UnixLister(ILister next) { nextInLine = next }

    @Override
    void listFiles(String dir) {
        if (System.getProperty('os.name') == 'Linux') {
            println "ls $dir".execute().text
        } else {
            nextInLine.listFiles(dir)
        }
    }
}

class WindowsLister implements ILister {
    private nextInLine

    WindowsLister(ILister next) { nextInLine = next }
    @Override
    void listFiles(String dir) {
        if (System.getProperty('os.name') == 'Windows XP') {
            println "cmd.exe /c dir $dir".execute().text
        } else {
            nextInLine.listFiles(dir)
        }
    }
}

class DefaultLister implements ILister {
    @Override
    void listFiles(String dir) {
        new File(dir).eachFile { f -> println f }
    }
}

def lister = new UnixLister(new WindowsLister(new DefaultLister()))

lister.listFiles(System.properties['user.home'] as String)
