package org.nodexy.groovyLang.designPatterns.abstractFactoryPattern

class TwoupFactory {
    def messages = TwoupMessages
    def control = TwoupControl
    def converter = TwoupInputConverter
}
