package org.nodexy.groovyLang.designPatterns.abstractFactoryPattern

class GuessFactory {
    def messages = GuessGameMessages
    def control = GuessGameControl
    def converter = GuessGameInputConverter
}
