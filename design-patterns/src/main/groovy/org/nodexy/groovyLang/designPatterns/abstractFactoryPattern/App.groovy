package org.nodexy.groovyLang.designPatterns.abstractFactoryPattern

class App {
    static void main(String[] args) {
        GameFactory.factory = new TwoupFactory()
        def messages = GameFactory.messages
        def control = GameFactory.control
        def converter = GameFactory.converter
        println messages.welcome
        def reader = new BufferedReader(new InputStreamReader(System.in))
        while (control.moreTurns()) {
            def input = reader.readLine().trim()
            control.play(converter.convert(input))
        }
        println messages.done
    }
}
